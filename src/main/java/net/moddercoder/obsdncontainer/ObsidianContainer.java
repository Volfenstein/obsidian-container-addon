package net.moddercoder.obsdncontainer;

import net.fabricmc.api.ModInitializer;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import net.moddercoder.obsdncontainer.boxes.Boxes;

import net.avongroid.expcontainer.ExperienceContainer;

public class ObsidianContainer implements ModInitializer {
	
	public static final Logger LOGGER = LogManager.getLogger();
	
	@Override
	public void onInitialize() {
		ExperienceContainer.REGISTER.registerBox(Boxes.OBSIDIAN_CONTAINER_BLOCK, "obsidian_box");
	}
}
