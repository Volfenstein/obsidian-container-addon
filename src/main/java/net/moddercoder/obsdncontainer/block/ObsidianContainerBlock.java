package net.moddercoder.obsdncontainer.block;

import java.util.Random;
import java.util.ArrayList;

import net.minecraft.world.World;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.block.Material;
import net.minecraft.block.BlockState;

import net.minecraft.item.Items;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import net.minecraft.sound.BlockSoundGroup;

import net.minecraft.particle.ParticleTypes;

import net.minecraft.server.world.ServerWorld;

import net.minecraft.entity.player.PlayerEntity;

import net.avongroid.expcontainer.api.ECBEHelper;

import net.avongroid.expcontainer.util.ExperienceStorage;

import net.moddercoder.obsdncontainer.reference.Reference;

import net.avongroid.expcontainer.block.ExperienceContainerBox;
import net.avongroid.expcontainer.block.ExperienceContainerBlock;

import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntity;

public class ObsidianContainerBlock extends ExperienceContainerBox {
	
	private static final Integer MINING_LEVEL = 3;
	
	public ObsidianContainerBlock() {
		super(FabricBlockSettings.of(Material.STONE)
			.luminance(ExperienceContainerBlock::defaultLightBehavior)
			.allowsSpawning(ExperienceContainerBlock::nonSpawning)
			.breakByTool(FabricToolTags.PICKAXES, MINING_LEVEL)
			.sounds(BlockSoundGroup.STONE)
			.strength(50f, 3600000f)
			.requiresTool()
		);
	}
	
	@Override
	public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
		if (!world.isClient) {
			ExperienceContainerBlockEntity xpContainerBlockEntity = ECBEHelper.get(world::getBlockEntity, pos);
			if (!ECBEHelper.isNull(xpContainerBlockEntity)) {
				if (!player.isSpectator() && !player.isCreative()) {
					ItemStack stack = player.getMainHandStack();
					if (stack.getItem() instanceof MiningToolItem) {
						MiningToolItem miningToolItem = (MiningToolItem)stack.getItem();
						if (miningToolItem.getMaterial().getMiningLevel() < MINING_LEVEL) {
							broke(world, pos, xpContainerBlockEntity);
						}
					} else {
						broke(world, pos, xpContainerBlockEntity);
					}
				}
			}
		}
		super.onBreak(world, pos, state, player);
	}
	
	private void broke (World world, BlockPos pos, ExperienceContainerBlockEntity xpContainerBlockEntity) {
		ExperienceStorage storage = xpContainerBlockEntity.getStorage();
		ArrayList<ItemStack> droppedStacks = new ArrayList<ItemStack>();
		
		for (int i = world.getRandom().nextInt(3); i > 0; i --) droppedStacks.add(Items.OBSIDIAN.getDefaultStack());
		for (int i = 3 + world.getRandom().nextInt(4); i > 0; i --) droppedStacks.add(Items.CRYING_OBSIDIAN.getDefaultStack());
		
		dropExperience((ServerWorld)world, pos, (int)((float)storage.getXP() * 0.8f));
		
		droppedStacks.forEach((s) -> dropStack(world, pos, s));
	}
	
	@Override
	public boolean hasDroppedExperienceWithoutSilkTouch(World world, ItemStack stack, BlockPos pos, BlockState state, PlayerEntity player) {
		return false;
	}
	
	@Override
	public boolean hasDroppedExperienceAfterExplode(World world, BlockPos pos, BlockState state) {
		return false;
	}
	
	@Override
	@Environment(EnvType.CLIENT)
	public void randomDisplayTick(BlockState state, World world, BlockPos pos, Random random) {
		if (random.nextInt(5) == 0) {
			Direction direction = Direction.random(random);
			if (direction != Direction.UP) {
				BlockPos blockPos = pos.offset(direction);
				BlockState blockState = world.getBlockState(blockPos);
				if (!blockState.isOpaque() || blockState.isSideSolidFullSquare(world, blockPos, direction.getOpposite())) {
					double dx = direction.getOffsetX() == 0 ? random.nextDouble() : 0.5d + (double)direction.getOffsetX() * 0.6d;
					double dy = direction.getOffsetY() == 0 ? random.nextDouble() : 0.5d + (double)direction.getOffsetY() * 0.6d;
					double dz = direction.getOffsetZ() == 0 ? random.nextDouble() : 0.5d + (double)direction.getOffsetZ() * 0.6d;
					world.addParticle(ParticleTypes.DRIPPING_OBSIDIAN_TEAR, (double)pos.getX() + dx, (double)pos.getY() + dy, (double)pos.getZ() + dz, 0d, 0d, 0d);
				}
			}
		}
	}
	
	@Override
	public BlockItem getBlockItem(net.minecraft.item.Item.Settings settings) {
		settings.rarity(Reference.OBSIDIAN_CONTAINER_RARITY);
		
		return new BlockItem(this, settings);
	}
	
	@Override
	public int getStorageCapacity() {
		return Reference.OBSIDIAN_CONTAINER_CAPACITY;
	}
	
}