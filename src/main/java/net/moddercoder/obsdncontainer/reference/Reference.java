package net.moddercoder.obsdncontainer.reference;

import net.minecraft.util.Rarity;

import net.avongroid.expcontainer.util.ExperienceStorageUtil;

public class Reference {
	
	public static final String VERSION = "1.0.2";
	public static final String AUTHOR = "Avongroid";
	
	public static final Rarity OBSIDIAN_CONTAINER_RARITY = Rarity.RARE;
	public static final int OBSIDIAN_CONTAINER_CAPACITY = ExperienceStorageUtil.getXPFromLevel(50);
	
}